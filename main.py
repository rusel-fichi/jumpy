import os
import random

import pygame as pg

import settings as conf
from sprites import Player, PlatformDinamicSpeed, SpriteSheet, Mob
from database_handler import DatabaseHandler


class Game(object):
    def __init__(self):
        pg.mixer.pre_init(44100, -16, 1, 512)
        pg.init()
        pg.display.set_caption("Jumpy Game")
        self.db = DatabaseHandler()
        self.base_dir = os.path.dirname(__file__)
        self.screen = pg.display.set_mode(conf.SIZE)
        bg = pg.image.load("assets/Background/bg-falling-night.png").convert()
        self.load_spritesheet_data()
        self.background = pg.transform.scale(bg, conf.SIZE)
        self.font_name = pg.font.match_font(conf.FONT_NAME)
        self.clock = pg.time.Clock()
        self.running = True
        self.playing = False
        self.playing = None

    def new(self):
        """
        Creates a new game
        """
        # constant definitions
        self.score = 0
        self.waiting = False
        self.mob_timer = pg.time.get_ticks()

        # assets
        self.jumping_sx = pg.mixer.Sound("assets/Sounds/Jumping/jump_11.wav")
        self.player_death = pg.mixer.Sound("assets/Sounds/cogs.ogg")

        # new platforms render from spritesheet
        self.platform_a_img = self.spritesheet.get_image(x=382, y=306, width=200, height=100)

        self.platform_b_img = self.spritesheet.get_image(x=420, y=1558, width=145, height=77)

        self.platform_c_img = self.spritesheet.get_image(x=382, y=204, width=200, height=100)

        self.floor_img = self.spritesheet.get_image(x=0, y=288, width=380, height=94)

        # create groups
        self.all_sprites = pg.sprite.LayeredUpdates()
        self.platforms = pg.sprite.Group()
        self.mobs = pg.sprite.Group()

        # create sprites
        self.player = Player(self)
        self.platform_ground = PlatformDinamicSpeed(self, conf.WIDTH / 2 - 100, conf.HEIGHT - 94, 1, self.floor_img)
        self.create_random_platforms()

        # start game
        self.run()

    def create_random_platforms(self):
        """
        Creates platforms at random positions in the screen
        """
        platform_characteristics = {
            "1": (self.platform_a_img, 0.5),
            "2": (self.platform_b_img, 2),
            "3": (self.platform_c_img, 1)
        }
        for i in range(8):
            x = random.randrange(5, conf.WIDTH - 60)
            y = random.randrange(20, conf.HEIGHT - 30)
            boost_platform = random.randrange(100) < 20
            platfom_selected = str(random.choice([1, 3]) if not boost_platform else 2)
            PlatformDinamicSpeed(self, x, y,
                                 platform_characteristics[platfom_selected][1],
                                 platform_characteristics[platfom_selected][0])

    def load_spritesheet_data(self):
        spritesheet_path = os.path.join(self.base_dir, "assets/spritesheets/spritesheet_jumper.png")
        self.spritesheet = SpriteSheet(spritesheet_path)

    def run(self):
        """
        Runs the game
        """
        pg.mixer.music.load(os.path.join(self.base_dir, "assets/Music/gameplay.ogg"))
        pg.mixer.music.play(loops=-1)
        self.playing = True
        while self.playing:
            self.clock.tick(conf.FPS)
            self.events()
            self.update()
            self.draw()
        pg.mixer.music.fadeout(500)

    def update(self):
        """
        Update everything in the screen
        """
        self.all_sprites.update()

        # spawning mobs
        now = pg.time.get_ticks()
        if now - self.mob_timer > conf.MOBS_FRECUENCY + random.choice([-2000, -1000, -500, 2000, 1000, 500]):
            self.mob_timer = now
            Mob(self)

        # check collisions with platforms
        if self.player.vel.y > 0:
            collision = pg.sprite.spritecollide(self.player, self.platforms, False)
            if collision:
                lowest_platform = collision[0]
                for plat in collision:
                    if plat.rect.bottom > lowest_platform.rect.bottom:
                        lowest_platform = plat

                if self.player.pos.y < lowest_platform.rect.bottom:
                    if self.player.pos.x < lowest_platform.rect.right + 10 and \
                            self.player.pos.x > lowest_platform.rect.left - 10:
                        self.jumping_sx.play()
                        speed_factor = getattr(next(iter(collision)), 'speed', 1)
                        self.player.jump(speed_factor)

        # check collision with mobs
        mob_hits = pg.sprite.spritecollide(self.player, self.mobs, False, pg.sprite.collide_mask)
        if mob_hits:
            self.player_death.play()
            self.playing = False

        # move camera
        if self.player.rect.top < conf.HEIGHT / 3:
            self.player.pos.y += abs(self.player.vel.y)
            for mob in self.mobs:
                mob.rect.y += abs(self.player.vel.y)
            for platform in self.platforms:
                platform.rect.y += abs(self.player.vel.y)
                if platform.rect.y >= conf.HEIGHT:
                    self.score += 10
                    platform.reposition()

        # kill platform ground
        if self.player.rect.top < conf.HEIGHT / 3 - 10:
            self.platform_ground.kill()

        # check if the player is dead
        if self.player.rect.bottom > conf.HEIGHT:
            for plat in self.platforms:
                plat.rect.y -= max(self.player.vel.y, 12)
                if plat.rect.bottom < 0:
                    plat.kill()
            if len(self.platforms) == 0:
                self.player_death.play()
                self.playing = False
                self.save_score()

    def events(self, event_type=None):
        """
        Handle events in the game
        :param event_type: Optional param to check for keydown event
        """
        for event in pg.event.get():
            if event.type == pg.QUIT:
                if self.playing:
                    self.playing = False
                self.running = False
            if event.type == event_type:
                self.waiting = False

    def draw(self):
        """
        Draw everything on the screen
        """
        self.screen.blit(self.background, conf.ORIGIN_POSITION)
        self.all_sprites.draw(self.screen)
        self.draw_text(f"Score: {self.score}", 20, conf.WHITE_COLOR, (conf.WIDTH / 2, 15))
        pg.display.flip()

    def draw_text(self, text: str, size: int, color: tuple, position: tuple):
        """
        Display a text in the screen
        :param text: text to be drawn
        :param size: text size
        :param color: text color
        :param position: position in the screen
        """
        font = pg.font.Font(self.font_name, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = position
        self.screen.blit(text_surface, text_rect)

    def save_score(self):
        """
        Save score into game.db in sqlite3
        """
        name = random.choice(["Luis", "GreatJumPY", "Micrrotox", "KLiller27"])
        self.db.insert_data(name, self.score)

    def draw_score(self):
        """
        Draw the top 5 highest score registered in the game
        """
        data = self.db.select_data_top()
        position = 30
        if data:
            self.draw_text(f"Score", 25, conf.WHITE_COLOR, (conf.WIDTH / 2, conf.HEIGHT / 3 - position + 20))
            for row in data:
                self.draw_text(text=f"{row[0]} ------ {row[1]}",
                               size=20,
                               color=conf.WHITE_COLOR,
                               position=(conf.WIDTH / 2, conf.HEIGHT / 3 + position))
                position += 30

    def waiting_for_player(self):
        """
        Keeps the player waiting endlessly until pressing down a key
        """
        self.waiting = True
        while self.waiting and self.running:
            self.events(event_type=pg.KEYDOWN)

    def display_startgame_screen(self):
        """
        Display the game start screen
        """
        pg.mixer.music.load(os.path.join(self.base_dir, "assets/Music/TremLoadingloopl.ogg"))
        pg.mixer.music.play(loops=-1)
        self.screen.blit(self.background, conf.ORIGIN_POSITION)
        self.draw_text("JUMPY GAME", 20, conf.WHITE_COLOR, (conf.WIDTH / 2, 100))
        self.draw_text("Press any key to start a new game", 20, conf.WHITE_COLOR, (conf.WIDTH / 2, conf.HEIGHT - 200))
        self.draw_score()
        pg.display.flip()
        self.waiting_for_player()
        pg.mixer.music.fadeout(500)

    def display_gameover_screen(self):
        """
        Display the game over screen
        """
        pg.mixer.music.load(os.path.join(self.base_dir, "assets/Music/dead_gameover.ogg"))
        pg.mixer.music.play(loops=-1)
        highest_score = self.db.select_data_top(limit=1)[0]
        self.screen.blit(self.background, conf.ORIGIN_POSITION)
        self.draw_text("GAME OVER", 20, conf.WHITE_COLOR, (conf.WIDTH / 2, conf.HEIGHT * 1 / 4))
        self.draw_text(f"Last score: {self.score}", 20, conf.WHITE_COLOR, (conf.WIDTH / 2, conf.HEIGHT * 1 / 2 - 30))
        if highest_score:
            self.draw_text(f"Highest score: {highest_score[1]}", 20, conf.WHITE_COLOR, (conf.WIDTH / 2,
                                                                                        conf.HEIGHT * 1 / 2 - 60))
        self.draw_text("Press any key to start a new game", 20, conf.WHITE_COLOR, (conf.WIDTH / 2, conf.HEIGHT * 2 / 3))
        pg.display.flip()
        self.waiting_for_player()
        pg.mixer.music.fadeout(100)


if __name__ == "__main__":
    g = Game()
    g.display_startgame_screen()
    while g.running:
        g.new()
        g.display_gameover_screen()
