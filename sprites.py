import random

import pygame as pg
from pygame import Vector2 as vec

import settings as conf


class Player(pg.sprite.Sprite):
    def __init__(self, game):
        self._layer = conf.PLAYER_LAYER
        self.groups = game.all_sprites
        super().__init__(self.groups)
        self.game = game
        self.walking = False
        self.jumping = False
        self.current_frame = 0
        self.last_update = 0
        self.load_images()
        self.image = self.standing_frames[0]
        self.image.set_colorkey(conf.BLACK_COLOR)
        self.rect = self.image.get_rect()
        self.rect.center = (conf.WIDTH / 2, conf.HEIGHT / 2)
        self.pos = vec((conf.WIDTH / 2, conf.HEIGHT / 2))
        self.acc = vec((0, 0))
        self.vel = vec((0, 0))

    def load_images(self):
        self.standing_frames = [self.game.spritesheet.get_image(x=614, y=1063, width=120, height=191),
                                self.game.spritesheet.get_image(x=690, y=406, width=120, height=201)]

        self.walking_frames_right = [self.game.spritesheet.get_image(x=678, y=860, width=120, height=201),
                                self.game.spritesheet.get_image(x=692, y=1458, width=120, height=207)]

        self.walking_frames_left = [pg.transform.flip(image, True, False) for image in self.walking_frames_right]

        self.jumping_frames = [self.game.spritesheet.get_image(x=614, y=1063, width=120, height=191),
                               self.game.spritesheet.get_image(x=382, y=763, width=150, height=181)]

    def set_walking_animation(self):
        if self.vel.x != 0:
            self.walking = True
        else:
            self.walking = False

    def set_jumping_animation(self):
        if self.vel.y != 0:
            self.jumping = True
        else:
            self.jumping = False

    def update_animation(self, frame_list):
        self.last_update = pg.time.get_ticks()
        self.current_frame = (self.current_frame + 1) % len(frame_list)
        self.image = frame_list[self.current_frame]
        self.image.set_colorkey(conf.BLACK_COLOR)
        self.mask = pg.mask.from_surface(self.image)
        bottom = self.rect.midbottom
        self.rect = self.image.get_rect()
        self.rect.midbottom = bottom

    def animate(self):
        self.set_walking_animation()
        self.set_jumping_animation()
        now = pg.time.get_ticks()
        if not self.walking and not self.jumping and now - self.last_update > 350:
            self.update_animation(self.standing_frames)

        if self.walking and not self.jumping and now - self.last_update > 200:
            if self.vel.x > 0:
                self.update_animation(self.walking_frames_right)
            else:
                self.update_animation(self.walking_frames_left)

        if self.jumping and now - self.last_update > 200:
            self.update_animation(self.jumping_frames)

    def update(self):
        self.animate()
        self.acc = vec(0, conf.PLAYER_GRAVITY)
        keys = pg.key.get_pressed()
        if keys[pg.K_LEFT]:
            self.acc.x = - conf.PLAYER_ACCELERATION
        if keys[pg.K_RIGHT]:
            self.acc.x = conf.PLAYER_ACCELERATION
        # if keys[pg.K_SPACE]:
        #     self.jump()

        self.acc.x += self.vel.x * conf.PLAYER_FRICTION
        self.vel += self.acc

        # check if velocity in x positive or x negative is less than 0.1, in that case it must be set to 0
        # if we dont set the x velocity to 0 it might never stay in 0 by itself (0.1 to infinitive)
        if abs(self.vel.x) < 0.7:
            self.vel.x = 0

        self.pos += self.vel + (0.5 * self.acc)
        if self.pos.x > conf.WIDTH:
            self.pos.x = 0
        if self.pos.x < 0:
            self.pos.x = conf.WIDTH
        self.rect.midbottom = self.pos

    def jump(self, speed=1):
        self.rect.y += 1
        hits = pg.sprite.spritecollide(self, self.game.platforms, False)
        self.rect.y -= 1
        if hits:
            self.vel.y = - conf.PLAYER_JUMP * speed


class PlatformDinamicSpeed(pg.sprite.Sprite):
    def __init__(self, game, x, y, speed, image):
        self._layer = conf.PLATFORM_LAYER
        self.groups = game.all_sprites, game.platforms
        super().__init__(self.groups)
        self.image = image
        self.image.set_colorkey(conf.BLACK_COLOR)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.speed = speed

    def reposition(self):
        # x = random.randrange(60, WIDTH - 60)
        # y = random.randrange(-600, - 10)
        # self.rect.x = x
        # self.rect.y = y
        width = random.randrange(50, 100)
        x = random.randrange(0, conf.WIDTH - width)
        y = random.randrange(-75, -30)
        self.rect.x = x
        self.rect.y = y


class SpriteSheet(object):
    def __init__(self, filename):
        self.sprite_sheet = pg.image.load(filename).convert()

    def get_image(self, x, y, width, height):
        image = pg.Surface((width, height))
        image.blit(self.sprite_sheet, conf.ORIGIN_POSITION, (x, y, width, height))
        image = pg.transform.scale(image, (int(width/2), int(height/2)))
        return image


class Mob(pg.sprite.Sprite):
    def __init__(self, game):
        self._layer = conf.MOB_LAYER
        self.groups = game.all_sprites, game.mobs
        super().__init__(self.groups)
        self.game = game
        self.image_up = self.game.spritesheet.get_image(x=566, y=510, width=122, height=139)
        self.image_up.set_colorkey(conf.BLACK_COLOR)
        self.image_down = self.game.spritesheet.get_image(x=568, y=1534, width=122, height=135)
        self.image_down.set_colorkey(conf.BLACK_COLOR)
        self.image = self.image_up
        self.rect = self.image.get_rect()
        self.vx = random.choice([1, 5])
        self.rect.y = random.randrange(conf.HEIGHT / 2)
        self.rect.centerx = random.choice([-100, conf.WIDTH + 100])
        if self.rect.x > conf.WIDTH:
            self.vx *= -1
        self.vy = 0
        self.dy = 0.5

    def update(self):
        self.rect.x += self.vx
        self.vy += self.dy
        if self.vy > 3 or self.vy < - 3:
            self.dy *= -1
        center = self.rect.center
        if self.dy < 0:
            self.image = self.image_up
        else:
            self.image = self.image_down
        self.mask = pg.mask.from_surface(self.image)
        self.rect = self.image.get_rect()
        self.rect.center = center
        self.rect.y += self.vy
        if self.rect.x > conf.WIDTH + 100 or self.rect.y < -100:
            self.kill()
