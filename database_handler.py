import datetime
import sqlite3


class DatabaseHandler(object):

    def __init__(self):
        self.table_name = "game"
        self.con = self.connection()
        self.initialize_database()

    @staticmethod
    def connection():
        con = sqlite3.connect("saved_data.db")
        return con

    def initialize_database(self):
        sql = f"SELECT name FROM sqlite_master WHERE type='table' AND name='{self.table_name}'"
        table_query = self.con.execute(sql)
        table_name = [row for row in table_query]
        if not table_name:
            self.create_table()

    def create_table(self):
        sql = f"CREATE TABLE {self.table_name}(id integer, name text PRIMARY KEY, score text, saved_date datetime_interval_code)"
        self.con.execute(sql)
        self.con.commit()

    def insert_data(self, name, score):
        saved_date = datetime.datetime.now()
        data = (name, str(score), saved_date)
        sql = f"INSERT INTO game(name, score, saved_date) VALUES(?,?,?)"
        try:
            self.con.execute(sql, data)
            self.con.commit()
        except sqlite3.IntegrityError as ex:
            self.update_data(name, score)

    def update_data(self, name, score):
        max_score = self.select_data(key="name", value=name, fields="max(score)")
        if int(max_score[0][0]) < score:
            sql = f'update {self.table_name} set score={score} where name="{name}"'
            self.con.execute(sql)
            self.con.commit()

    def select_data(self, key=None, fields=None, value=None, limit=0, order="desc"):
        fields = fields if fields else '*'
        where = f"where {key}='{value}'" if key and value else ''
        limit = f"limit {limit}" if limit else ''

        sql = f"select {fields} from {self.table_name} {where} order by score {order} {limit}".replace("\'", '"')
        results = self.con.execute(sql)

        return [row for row in results]

    def select_data_top(self, limit=5):
        data = self.select_data(fields="name, score", limit=limit)
        return data

    def select_data_by_name(self, key, value):
        data = self.select_data(fields="name, score", key=key, value=value)
        return data
